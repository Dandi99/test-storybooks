import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import HeaderNav from './headerNav';

export default {
  title: 'Header',
  component: HeaderNav,
};

export const header = () => ({
  components: { HeaderNav },
  template: '<headerNav />',
  methods: { action: action('clicked') },
});

