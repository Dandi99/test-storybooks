import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import Vue from 'vue'

import ShowBlogs from './showBlogs';
import vueResource from 'vue-resource'

Vue.use(vueResource)

export default {
  title: 'show blog',
  component: ShowBlogs,
};

export const All = () => ({
  components: { ShowBlogs },
  template: '<showBlogs :Num="5"/>',
  methods: { action: action('clicked') },
});

export const Empty = () => ({
  components: { ShowBlogs },
  template: '<showBlogs :Num="0"/>',
  methods: { action: action('clicked') },
});


