import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import Vue from 'vue'

import AddBlog from './addBlog';
import vueResource from 'vue-resource'

Vue.use(vueResource)

export default {
  title: 'add blog',
  component: AddBlog,
};

export const Uncomplete = () => ({
  components: { AddBlog },
  template: '<addBlog />',
  methods: { action: action('clicked') },
});

export const Complete = () => ({
  components: { AddBlog },
  template: '<addBlog :submitted="true"></addBlog>',
  methods: { action: action('clicked') },
});
